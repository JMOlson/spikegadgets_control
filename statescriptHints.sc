Triggers can not be called anywhere except for in nothing or callbacks - they cannot be in other triggers. They cannot be in if statements, even if in a callback.

I'm not sure if you can use a variable as an input to trigger, i.e.
int a = 1
trigger(a)

I'll need to try it.
