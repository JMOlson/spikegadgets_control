#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
A simple graphical output of statescript data for use with the AdaptAMaze platform

License:
BSD-3-Clause
"""
# Built-in/Generic Imports

# Libs
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Own modules

__author__ = 'Jacob M Olson'
__copyright__ = 'Copyright 2021, Jacob M Olson'
__credits__ = ['Jacob M Olson']
__license__ = 'BSD-3-Clause'
__version__ = '0.0.1'
__maintainer__ = 'Jacob M Olson'
__email__ = 'jolson1129@gmail.com'
__status__ = 'dev'


# TODO TODO TODO
# for plotting. make this better later
COLOR_LIST = ['c', 'm', 'y', 'k']

class StatescriptGui:
    '''A simple statescript GUI for use with the AdaptAMaze platform'''

    def __init__(self, nWells=0):
        '''Create the graphical output window.'''
        self.mainGraphWindow = plt.figure(constrained_layout=True)
        self.mainGrid = gridspec.GridSpec(3, 3)
        self.rewardTimesAx = self.mainGraphWindow.add_subplot(self.mainGrid[2, 0:-1])
        self.rewardTimesAx.set_title('Reward Times')
        self.lickDistAx = self.mainGraphWindow.add_subplot(self.mainGrid[0:-1, 2])
        self.lickDistAx.set_title('Lick Widths')
        self.nWells = nWells
        plt.draw()
        print('did we make it?')
    
    def setNWells(self, nWells):
        self.nWells = nWells
        self.createWellAxes()
            
    def createWellAxes(self):
        self.lickCountGrid = gridspec.GridSpecFromSubplotSpec(1, self.nWells+1, self.mainGrid[0, 0:-1])
        #lickCountAx.set_title('Lick Counts')
        
        self.rewardCountGrid = gridspec.GridSpecFromSubplotSpec(1, self.nWells+1, self.mainGrid[1, 0:-1])
        #rewardCountAx.set_title('Reward Counts')
        
        self.lickCountAxList = []
        self.rewardCountAxList = []
        for iWell in range(int(self.nWells+1)):
            self.lickCountAxList.append(plt.Subplot(self.mainGraphWindow, self.lickCountGrid[0, iWell]))
            self.mainGraphWindow.add_subplot(self.lickCountAxList[iWell])
            self.rewardCountAxList.append(plt.Subplot(self.mainGraphWindow, self.rewardCountGrid[0, iWell]))
            self.mainGraphWindow.add_subplot(self.rewardCountAxList[iWell])
    
    def updateGui(self, RecData, eventType):
        if eventType == 'licks':
            self.updateLickCounts(RecData.getNLicks())
            print['hi']
        elif eventType == 'reward':
            self.updateRewardCounts(RecData.getNRewards())
            self.updateRewardDistGraph(RecData.getRewardDist())
            self.updateLickDistGraph(RecData.getLickDurations())
            print['ho']
        else:
            pass
        plt.show()
            
    def updateLickDistGraph(self, lickDurationTuple):
        lickDurations = lickDurationTuple[0]
        self.lickDistAx.hist(lickDurations)
    
    def updateRewardDistGraph(self, rewardTimeTuple):
        rewardTimes = rewardTimeTuple[0]
        portIDs = rewardTimeTuple[1]
        for aPort in set(portIDs):
            self.rewardTimesAx.stem(rewardTimes[portIDs == aPort], aPort, 
                                    COLOR_LIST[aPort], 
                                    markerfmt=COLOR_LIST[aPort],
                                    label=str(aPort))
    
    def updateLickCounts(self, lickCountTuple):
        lickCounts = lickCountTuple[0]
        portIDs = lickCountTuple[1]
        for i, aPort in enumerate(portIDs):
            thisAx = plt.sca(self.lickCountAxList[i])
            thisAx.axis([0, 1, 0, 1])
            thisAx.text(0.25, 0.25, str(lickCounts(i)), style='bold',
                        bbox={'facecolor': 'gray', 'alpha': 0.5, 'pad': 10})
    
    def updateRewardCounts(self, rewardCountTuple):
        rewardCounts = rewardCountTuple[0]
        portIDs = rewardCountTuple[1]
        for i, aPort in enumerate(portIDs):
            thisAx = plt.sca(self.rewardCountAxList[i])
            thisAx.axis([0, 1, 0, 1])
            thisAx.text(0.25, 0.25, str(rewardCounts(i)), style='bold',
                        bbox={'facecolor': 'gray', 'alpha': 0.5, 'pad': 10})
    
