#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Log recording and control status GUI during AdaptAMaze recs fully controlled
by statescript protocols.

There are 3 basic line types, Code, messages, and input/output events.
These are called 'CODE', 'MESSAGE', and 'IOEVENT' when passing around line
info. This must be consistent with the callback used.

License:
BSD-3-Clause
"""


# Built-in/Generic Imports

# Libs
import numpy as np
import pandas as pd

# Own modules

__author__ = 'Jacob M Olson'
__copyright__ = 'Copyright 2021, Jacob M Olson'
__credits__ = ['Jacob M Olson']
__license__ = 'BSD-3-Clause'
__version__ = '0.0.1'
__maintainer__ = 'Jacob M Olson'
__email__ = 'jolson1129@gmail.com'
__status__ = 'dev'

"""
 I want a recording file object.
 Eventually, that object have another class for each recording file type -
 e.g., adaptamaze probably has one object type, other mazes could have another.

Properties
Code (List of lines of code read, time?)
Events
Messages

event info:
timeStart
timeEnd
eventType ('input', 'output')
eventDuration
portID
"""


class StateScriptRec:
    IO_MAPPINGS = {'input': {True: ('ECU_Din', 'high'),
                             False: ('ECU_Din', 'low')},
                   'output': {True: ('ECU_Dout', 'high'),
                              False: ('ECU_Dout', 'low')}}

    def __init__(self, nInputs=32, nOutputs=32):
        self.fullLog = list([])
        self.code_lines = list([])
        self.message_lines = list([])
        self.event_lines = list([])
        self.line_type = list([])

        self.n_input_events = np.zeros([nInputs+1])  # The +1 is for the total.
        self.n_output_events = np.zeros_like(self.n_input_events)

        self.eventStart_ms = list([])
        self.eventEnd_ms = list([])
        self.eventDuration_ms = list([])
        self.eventPortID = list([])
        self.eventHardwareType = list([])
        self.eventType = list([])

        self.ecuState = {'time_ms': 0,
                         'inputState': np.zeros([nInputs, 1]),
                         'outputState': np.zeros([nOutputs, 1])}

    @classmethod
    def from_file(cls, filename, nInputs=32, nOutputs=32):
        '''
        Constructor - initialize StatescriptRecData object from a saved
        statescript log.

        Parameters
        ----------
        fileName : str
            Name of StateScript output file.
        See main constructor for other parameters.
        '''
        SSRD = cls(nInputs=nInputs, nOutputs=nOutputs)
        SSRD.__import_statescript(filename)
        return SSRD

    @classmethod
    def from_string(cls, contents, nInputs=32, nOutputs=32):
        '''
        Constructor - initialize StatescriptRecData object from a saved
        statescript log.

        Parameters
        ----------
        contents : str
        contents of StateScript output file.
        See main constructor for other parameters.
        '''
        SSRD = cls(nInputs=nInputs, nOutputs=nOutputs)
        SSRD.__read_statescript(contents)
        return SSRD

    def __import_statescript(self, fileName):
        '''
        Import StateScript output file.
        '''
        with open(fileName) as f:
            raw_contents = f.readlines()
        self.__read_statescript(raw_contents)
        return

    def __read_statescript(self, fileContents):
        '''
        Read StateScript output file into list of strings.
        '''
        if type(fileContents) is list:
            [self.recordLine(i) for i in fileContents]
        elif type(fileContents) is str:
            [self.recordLine(i) for i in fileContents.splitlines()]
        else:
            raise TypeError(
                'fileContents must be a string or list of strings.')
        return

    def recordLine(self, line):
        '''
        Record a line of statescript output to the object.
        '''
        self.fullLog.append(line)
        lineType = self.determineLineType(line)
        self.line_type.append(lineType)
        if lineType == 'CODE':
            self.code_lines.append(line)
            eventTypes = []
        elif lineType == 'MESSAGE':
            self.message_lines.append(line)
            eventTypes = []
        elif lineType == 'IOEVENT':
            self.event_lines.append(line)
            eventTypes = self.recordEvents(line)
        else:
            raise NotImplementedError(
                'Unknown line type - please implement how to respond')
        return eventTypes

    @staticmethod
    def determineLineType(line):
        '''
        Return a string - 'CODE', 'MESSAGE', or 'IOEVENT'
        '''
        words = line.split()
        if not words[0].isnumeric():
            lineType = 'CODE'
        elif words[1].isnumeric():
            lineType = 'IOEVENT'
        else:
            lineType = 'MESSAGE'
        return lineType

    def recordEvents(self, line):
        currentState = self.parseEventLine(line)
        eventTypes = []
        if self.ecuState['time_ms'] != 0:
            newEvents = self.extractEvents(self.ecuState, currentState)
            for i, iEvent in enumerate(newEvents):
                self.respondToEvent(iEvent)
                eventTypes.append(iEvent['eventType'])
        self.ecuState = currentState
        return eventTypes

    @classmethod
    def parseEventLine(cls, line):
        words = line.split()
        lineData = {'time_ms': int(words[0]),
                    'inputState': cls.convertStrToBinVec(words[1]),
                    'outputState': cls.convertStrToBinVec(words[2])}
        return lineData

    @staticmethod
    def convertStrToBinVec(integerStr, nBits=32, bitFlip=True):
        nBitsStr = '0' + str(nBits) + 'b'  # format string for 0 padded binary
        binVec = np.array([int(x) for x in format(int(integerStr), nBitsStr)])
        if bitFlip:
            binVec = np.flip(binVec)
        return binVec

    @classmethod
    def extractEvents(cls, oldLineData, newLineData):
        'Find differences in state and turn into event dict data'
        eventList = []
        inputEvents = cls.stateCompare(
            oldLineData['inputState'], newLineData['inputState'])
        outputEvents = cls.stateCompare(
            oldLineData['outputState'], newLineData['outputState'])
        if len(inputEvents[0]):
            eventList = eventList + \
                cls.makeEventDicts(
                    inputEvents, newLineData['time_ms'], 'input')
        if len(outputEvents[0]):
            eventList = eventList + \
                cls.makeEventDicts(
                    outputEvents, newLineData['time_ms'], 'output')
        return eventList

    @staticmethod
    def stateCompare(firstStateVec, secondStateVec):
        'Compare two state vectors, find differences (events)'
        eventDiffs = secondStateVec - firstStateVec
        # find non zero values, save locations and values
        eventChannels = eventDiffs.nonzero()[0]
        eventTypes = eventDiffs[eventChannels]
        return [eventChannels, eventTypes]

    @classmethod
    def makeEventDicts(cls, eventData, time_ms, ioState):
        'turn event list into a list of event dicts'
        eventPorts = eventData[0]
        changeDirs = eventData[1]
        events = []
        for i, v in enumerate(eventPorts):
            events.append(
                {'time_ms': time_ms,
                 'portID': v+1,  # +1 to make portID 1 indexed
                 'eventType': cls.IO_MAPPINGS[ioState][changeDirs[i] > 0]})
        return events

    def respondToEvent(self, event):
        '''Update data object with data from each event. '''
        if 'high' in event['eventType'][1]:
            self.addEvent(event)  # beginning of event - create new entry
        elif 'low' in event['eventType'][1]:
            self.updateEvent(event)  # end of event - fill out properties
        else:
            raise NotImplementedError(
                'Unknown event type - implement how to respond')

    def addEvent(self, event):
        self.eventStart_ms.append(event['time_ms'])
        self.eventEnd_ms.append(0)
        self.eventDuration_ms.append(0)
        self.eventPortID.append(event['portID'])
        self.eventType.append(event['eventType'][0])
        if event['eventType'][0] == 'input':
            self.n_input_events[event['portID']] += 1
            self.n_input_events[0] += 1
        elif event['eventType'][0] == 'output':
            self.n_output_events[event['portID']] += 1
            self.n_output_events[0] += 1

    def updateEvent(self, event):
        # find the event to update
        thisEvent = next(
            (i for i in reversed(range(len(self.eventStart_ms)))
             if (self.eventPortID[i] == event['portID']
                 and self.eventType[i] == event['eventType'][0])), False)
        if thisEvent:
            self.eventEnd_ms[thisEvent] = event['time_ms']
            self.eventDuration_ms[thisEvent] = (
                self.eventEnd_ms[thisEvent] - self.eventStart_ms[thisEvent])

    # Getters
    def get_lines(self, line_number=None):
        '''
        Get line number line_number.
        '''
        if line_number is None:
            return self.fullLog
        else:
            return self.fullLog[line_number]

    def get_event_lines(self):
        '''
        Get IO event lines.
        '''
        return self.event_lines

    def get_message_lines(self):
        '''
        Get message lines.
        '''
        return self.message_lines

    def get_code_lines(self):
        '''
        Get code lines.
        '''
        return self.code_lines

    def get_line_types(self, line_number=None):
        '''
        Get line type of specified line numbers.
        '''
        if line_number is None:
            return self.line_type
        else:
            return self.line_type[line_number]

    def get_input_event_counts(self, port=-1):
        if port == -1:
            # Find the last nonzero value in self.nPortEntries.
            # Only return the list of nonzero values.
            nPorts = np.nonzero(self.n_output_events)[0]
            return self.n_input_events[:nPorts[-1]+1]
        else:
            return self.n_input_events[port]

    def get_output_event_counts(self, port=-1):
        if port == -1:
            # Find the last nonzero value in self.n_output_events.
            # Only return the list of nonzero values.
            nPorts = np.nonzero(self.n_output_events)[0]
            return self.n_output_events[:nPorts[-1]+1]
        else:
            return self.n_output_events[port]

    def get_variables(self, var_list):
        '''
        Get variables from code. Gets first instance of variable_name.
        We care about the first instance because that is the initial
        condition.

        Takes a string or a list of strings.

        Returns a dictionary of variable names and values.
        '''

        if isinstance(var_list, list):
            var_strs = [self._get_variable(iVar) for iVar in var_list]
        elif isinstance(var_list, str):
            var_strs = [self._get_variable(var_list)]
        else:
            raise ValueError(
                'variables must be a string or a list of strings.')
        var_dict = {i_var.split(' ')[1]: i_var.split(' ')[-1]
                    for i_var in var_strs if i_var is not None}
        return var_dict

    def _get_variable(self, variable_name):
        for line in self.code_lines:
            if variable_name in line:
                return line
        return None

    def get_messages(self):
        '''
        Return message dataframe. First column is time, second column is
        the message.
        '''
        split_lines = [x.split(' ', 1) for x in self.message_lines]
        message_df = pd.DataFrame(data=[x[1] for x in split_lines],
                                  index=[int(x[0]) for x in split_lines])
        return message_df

    def _get_events(self):
        '''
        Return event dataframe.
        '''
        id = [type + str(id)
              for type, id in zip(self.eventType, self.eventPortID)]
        return pd.DataFrame({
            'id': id,
            'io_type': self.eventType,
            'io_type_id': self.eventPortID,
            'start_time': np.array(self.eventStart_ms) / 1000,
            'stop_time': np.array(self.eventEnd_ms) / 1000,
            'duration': np.array(self.eventDuration_ms) / 1000
        })

    def get_events(self, filter=None):
        '''
        filter is for event_type. Should expand to be key value pairs
        to extend the capabilities in the future.
        '''
        all_events = self._get_events()

        if filter is not None:
            # event_types = np.unique(self.eventType)
            return all_events[(all_events['event_type'] == filter)]
        return all_events

    @staticmethod
    def filter_events(events_df, filter):
        '''
        filter is for id. Should expand to be key value pairs
        to extend the capabilities in the future.

        As is, if a string, filters on that string. if a list of strings,
        filters on each item in the list.
        '''
        if isinstance(filter, str):
            return events_df[(events_df['id'] == filter)]
        elif (isinstance(filter, list) and
              all([isinstance(item, str) for item in filter])):
            new_event_df = pd.concat(
                [events_df[(events_df['id'] == item)] for item in filter])
            return new_event_df.sort_index()
        else:
            # add type error here
            return False

    # Save Method

    def save(self, saveFileName=''):
        import tkinter as tk
        from tkinter import filedialog
        if saveFileName.isempty():
            # next 2 lines ensure an extra window doesn't pop up.
            root = tk.Tk()
            root.withdraw()
            fileTypes = [('Pandas Format', '*.pkl'),
                         ('Zip', '*.zip'),
                         ('GZip', '*.gz'),
                         ('All Files', '*.*')]
            saveFileName = filedialog.asksaveasfile(
                title='Please select where to save your dataset.',
                filetypes=fileTypes, defaultextension=fileTypes
            )
            root.destroy()  # closes the extraneous window.
        RecData = pd.DataFrame({'EventType': self.eventType,
                               'EventHardwareType': self.eventHardwareType,
                                'Port': self.eventPortID,
                                'StartTime_ms': self.eventStart_ms,
                                'EndTime_ms': self.eventEnd_ms,
                                'Duration_ms': self.eventDuration_ms})
        RecData.to_pickle(saveFileName)
