#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Log recording and control status GUI during AdaptAMaze recs fully controlled
by statescript protocols.

There are 3 basic line types, Code, messages, and input/output events.
These are called 'CODE', 'MESSAGE', and 'IOEVENT' when passing around line info.
This must be consistent with rec data class used.

License:
BSD-3-Clause
"""
# Built-in/Generic Imports

# Libs

from StatescriptGui import StatescriptGui as SSGui
# Own modules
from StateScriptRecData import StateScriptRec as SSRecData

__author__ = 'Jacob M Olson'
__copyright__ = 'Copyright 2021, Jacob M Olson'
__credits__ = ['Jacob M Olson']
__license__ = 'BSD-3-Clause'
__version__ = '0.0.1'
__maintainer__ = 'Jacob M Olson'
__email__ = 'jolson1129@gmail.com'
__status__ = 'dev'


def callback(line):
    ''' This is the custom callback function. When events occur, addScQtEvent will
    call this function. This function MUST BE NAMED 'callback'!!!!'''
    eventTypes = RecData.recordLine(line)
    if eventTypes and eventTypes[0][0] == 'output' and eventTypes[0][1] == 'low':
        # updateType = 'reward'
        rewCounts = RecData.get_output_event_counts()
        print('Total Rewards: {}'.format(rewCounts[0]))
        [print('Well {}: {}'.format(iWell+1, nRews))
         for iWell, nRews in enumerate(rewCounts[1:])]
    else:
        pass
    # RecStatusGui.updateGui(RecData, updateType)
        # updateType = 'licks'

def ifwewantit(event):
    if event['eventType'].startswith('beam'):
        event['eventType'] = 'VISIT'
    elif event.startswith('reward'):
        event['eventType'] = 'REWARD'
    else:
        raise NotImplementedError('Unknown event type - implement how to respond')


# Runs on select in statescript program - We'll want to initialize GUI & recording file object
# if __name__ == '__main__':
RecData = SSRecData()
print("Rec Data Loaded")
# RecStatusGui = SSGui(nWells)
