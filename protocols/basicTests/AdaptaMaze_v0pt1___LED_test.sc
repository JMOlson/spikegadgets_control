%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%  Name:  LED Test
%%	Author: Jacob Olson
%%  Purpose: Test well led communication/fn by flickering on/off
%%           Ideal for linear track on AdaptaMaze.
%%
%%  Notes:
%%    Doesn't currently cycle - just light one. trying to get this, then will figure out the cycle.
%%
%%  Version: 0.2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIALIZE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------
% Adjustable Parameters
% -------------------------

int nWellsToTest = 4
int blinkLength = 500
int nBlinks = 10

% -------------------------
% Hardware & Analog Communication Parameters
% -------------------------
int nLEDs = 2
int nWells = 16
int mVPerMapStep = 250
int writeTime = 2

% -------------------------
% Analog Output Assignments
% -------------------------
% LEDs are multiplexed - so LED1s is all 16 wells' LED1, and so on.
int led1s = 1
int led2s = 2

% -------------------------
% LED Multiplex Control and Lookup Table
% -------------------------
% Toggles LED on/off of well corresponding to the voltage below.
% Reads every 1ms, but every 5ms after receiving a value,
% so we should do writes for only 2ms
%
% 0V is resting value when not transmitting.
%  1 = 0.25
%  2 = 0.5
%  3 = 0.75
%  4 = 1.0
%  5 = 1.25
%  6 = 1.5
%  7 = 1.75
%  8 = 2.0
%  9 = 2.25
% 10 = 2.5
% 11 = 2.75
% 12 = 3.0
% 13 = 3.25
% 14 = 3.5
% 15 = 3.75
% 16 = 4.0

% -------------------------
% Behavior Trackers
% -------------------------

int wellCount = 1
int aOutSelected = 1
int iBlink = 0
int nBlinkChanges = nBlinks + nBlinks
nBlinkChanges = nBlinkChanges - 1
int blinkLengthMinusTen = blinkLength - 10

%% ZERO OUT THE CLOCK! %%
clock(reset)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HELPER FUNCTIONS (executed with ‘trigger(X)’, where X is the fn #)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% cycle through LEDs, turning on/off
function 1
wellCount = 1
% Turn on LED 1
aOutSelected = led1s
analogout[led1s] = 250 % wellCount * mVPerMapStep
do in blinkLength
  % cycle through all the wells to test
  while wellCount <= nWellsToTest do every blinkLength

    % Blink one light on and the other off
    analogout[led1s] = 250 % wellCount * mVPerMapStep
    analogout[led2s] = 250 % wellCount * mVPerMapStep
    do in writeTime
      analogout[led1s] = 0
      analogout[led2s] = 0
    end
    iBlink = iBlink+1

    % on to the next well after nBlinks of each LED
    if (iBlink == nBlinkChanges) do
      wellCount = wellCount+1
      iBlink = 0
      disp('On to well')
      disp(wellCount)

      % turn off LED 2
      do in blinkLengthMinusTen % worried if exactly at same time may interfere...
        analogout[led2s] = 250 % wellCount * mVPerMapStep
        do in writeTime
          analogout[led2s] = 0
        end
      end
    end
  then do
  end
end
end;

%%%%%%%%%%%%%%%%%%%%%%%%%
%%   CALLBACK SECTION
%%%%%%%%%%%%%%%%%%%%%%%%%
