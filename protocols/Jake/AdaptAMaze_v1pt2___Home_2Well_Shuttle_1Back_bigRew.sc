%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%  Name:  Home + 2 Well - Shuttle - 1-Back - Lick Detect Well Entry
%%	Author: Jacob Olson
%%  Purpose: Standard W or T maze alternation (1-back) task: 3 wells.
%%           Reward when visited in 1 -> 2 -> 3 -> 2 ... pattern.
%%           Well visit determined by an adequate number of licks.
%%           Ideal for W or T 3 well alternation task on AdaptaMaze.
%%
%%  Reminder: All time values are in ms.
%%
%%  Notes:
%%    Beam status is low when unoccupied, high when broken, and low again when the beam break ends.
%%    Pump #s and beambreak #s correspond to the same wells.
%%	V1.2 now uses smaller home rewards.
%%
%%   Version: 1.2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Example Track and Well Layout (2 is home well)
%  ___ ___
% |	  |	  |
% |   |	  |
% |1  |2  |3

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIALIZE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------
% Adjustable Parameters
% -------------------------
int nLicksForWellEntry = 2

% beambreak duration window that will register as a lick
int lickDurationMin = 25
int lickDurationMax = 150

% pump run time - my (JMO) Jadhav Lab pumps are set to dispense at a rate of 15 ml/mn = 0.25 ml/sec
int rewPumpOnTime_Away = 500 % 0.075 ml
int rewPumpOnTime_Home = 375 % 0.0625 ml
int rewPumpOnTime = 0 

int homeWellID = 2

% -------------------------
% Parameters for triggering pumps on demand
% -------------------------
int pumpToTrig = 0

% -------------------------
% Behavior Trackers
% -------------------------
int well1RewReady = 1
int well3RewReady = 1

int outsideRewReady = 1
int homeRewReady = 1
int currWellRewReady = 1

int lastRewWell = 0
int lickWell = 0
int currWell = 0 % used as a variable so all well callbacks are the same

int iReward = 0
int dispenseReward = 0

int beambreakDuration = 0
int beambreakStart = 0
int beambreakEnd = 0

int lickCount = 0
int nonLickCount = 0

%% ZERO OUT THE CLOCK! %%
clock(reset)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HELPER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% check if beambreak is a lick, add to lick or nonlick counts
function 1
beambreakDuration = beambreakEnd - beambreakStart
if (beambreakDuration > lickDurationMin && beambreakDuration < lickDurationMax) do
  % was a lick
  lickCount = lickCount+1
else do
  nonLickCount = nonLickCount+1
end
end;

%%%%%%%%%%%%%%%%%%%%
% well reward logic and visit tracking handled here for each event
function 2
if (lickCount == nLicksForWellEntry && currWellRewReady == 1) do
  dispenseReward = 1
  currWellRewReady = 0
else do
  dispenseReward = 0
end
end;

%%%%%%%%%%%%%%%%%%%%
% trigger reward if ready
function 3
if (dispenseReward == 1) do
  iReward = iReward+1
  portout[lickWell] = 1  % Administer reward
  dispenseReward = 0
  % output current program status:
  disp(lickWell)
  disp(iReward)
  rewPumpOnTime = rewPumpOnTime_Away
  if (lickWell == homeWellID) do
	rewPumpOnTime = rewPumpOnTime_Home
  end
  do in rewPumpOnTime % after the appropriate time
    portout[lickWell] = 0 % turn pump off
    lastRewWell = lickWell
  end

end
end;

%%%%%%%%%%%%%%%%%%%%
% output well interactions to screen
function 4
disp('Well #')
disp(lickWell)
disp('Left after:')
disp(lickCount)
disp(nonLickCount)

% reset things for this well
lickCount = 0
nonLickCount = 0
lickWell = currWell
disp(well1RewReady)
disp(well3RewReady)
disp(outsideRewReady)
disp(homeRewReady)
end;

%%%%%%%%%%%%%%%%%%%%
% if well is officially visited, reset home/goal rew states
function 6
if (lickCount == nLicksForWellEntry) do
  if (lickWell == homeWellID) do
    outsideRewReady = 1
    homeRewReady = 0
  else do
    outsideRewReady = 0
    homeRewReady = 1
    if (lickWell == 1) do
      well1RewReady = 0
      well3RewReady = 1
    else do
      well1RewReady = 1
      well3RewReady = 0
    end
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% User control of pumps - 50 is last fn
function 50
portout[pumpToTrig] = 1  % Trigger pump
rewPumpOnTime = rewPumpOnTime_Away
if (pumpToTrig == homeWellID) do
	rewPumpOnTime = rewPumpOnTime_Home
end
do in rewPumpOnTime % after the appropriate time
    portout[pumpToTrig] = 0 % turn pump off
end
end;

%%%%%%%%%%%%%%%%%%%%%%%%%
%%   CALLBACK SECTION
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% Beambreak Up %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 1
callback portin[1] up
currWell = 1
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  if ((well1RewReady == 1) && (outsideRewReady == 1)) do
    currWellRewReady = 1
  else do
    currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 2
callback portin[2] up
currWell = 2
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  currWellRewReady = homeRewReady
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 3
callback portin[3] up
currWell = 3
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  if ((well3RewReady == 1) && (outsideRewReady == 1)) do
    currWellRewReady = 1
  else do
    currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%% Beambreak Down %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 1
callback portin[1] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward == 1 do
  well1RewReady = 0
end
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 2 - HOME WELL
callback portin[2] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  homeRewReady = 0
end
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 3
callback portin[3] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  well3RewReady = 0
end
trigger(3) % dispense reward if necessary
end;

%% Trigger 50 for pump run. pumpToTrig is the var to choose which pump.
