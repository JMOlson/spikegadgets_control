%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%  Name:  2 Well Alternation - Transition to Lick Detect Well Entry
%%	Author: Jacob Olson
%%  Purpose: 2 wells, reward when visited in alternation. Well visit determined
%%           by an adequate number of licks. For first 20 trials, half reward
%%           amount is delivered, but opposite well fills when well is visited
%%           (so reward is waiting for animal). After, well fills when it is
%%           visited.
%%           Ideal for linear track on AdaptaMaze.
%%
%%  Quirks: Licks needed start at a lower value and increase +1 each trial until normal setting is reached.
%%  Reminder: All time values are in ms.
%%
%%  Notes:
%%    Beam status is low when unoccupied, high when broken, and low again when the beam break ends.
%%    Pump #s and beambreak #s correspond to the same wells.
%%
%%  Version: 1.1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIALIZE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------
% Parameters for triggering pumps on demand
% -------------------------
int pumpToTrig = 0

% -------------------------
% Adjustable Parameters
% -------------------------
int nLicksForWellEntry = 2
int lickForWellEntryThisTrial = 1 % increase over first few trials to the number above

% beambreak duration window that will register as a lick
int lickDurationMin = 25
int lickDurationMax = 250

% pump run time - my (JMO) Jadhav Lab pumps are set to dispense at a rate of 15 ml/mn = 0.25 ml/sec
int rewPumpOnTime = 300 % 0.075 ml
int halfPumpOnTime = 300

% number of rewards before going back to normal pump on time.
int nTransitionTrials = 5

% -------------------------
% Behavior Trackers
% -------------------------

int lastRewWell = 0
int oppWell = 0
int lickWell = 0
int currWell = 0 % used as a variable so all well callbacks are the same

int currTime = 0
int iReward = 0
int dispenseReward = 0

int beambreakDuration = 0
int beambreakStart = 0
int beambreakEnd = 0

int lickCount = 0
int nonLickCount = 0

%% ZERO OUT THE CLOCK! %%
clock(reset)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HELPER FUNCTIONS (executed with ‘trigger(X)’, where X is the fn #)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% check if beambreak is a lick, add to lick or nonlick counts
function 1
beambreakDuration = beambreakEnd - beambreakStart
if (beambreakDuration > lickDurationMin && beambreakDuration < lickDurationMax) do
  % was a lick
  lickCount = lickCount+1
else do
  nonLickCount = nonLickCount+1
end
end;

%%%%%%%%%%%%%%%%%%%%
% determine if reward should be given for this event
function 2
if (lickCount == lickForWellEntryThisTrial && lastRewWell != lickWell) do
  dispenseReward = 1
else do
  dispenseReward = 0
end
end;

%%%%%%%%%%%%%%%%%%%%
% trigger reward if ready
function 3
% pretransition (prefill) reward delivery
if (dispenseReward == 1 && iReward < nTransitionTrials) do
  iReward = iReward+1
  portout[oppWell] = 1  % Administer reward
  % output current program status:
  disp('Animal receiving reward at')
  disp(lickWell)
  disp('Reward dispensed at ')
  disp(oppWell)
  disp(iReward)
  if (lickForWellEntryThisTrial < nLicksForWellEntry) do
    lickForWellEntryThisTrial = lickForWellEntryThisTrial+1
  end
  do in halfPumpOnTime % trying to transition to little or no reward, so animal just licks either way.
    portout[oppWell] = 0 % turn pump off
    lastRewWell = lickWell % where animal last got reward, not where dispensed - keeps the logic the same for fn 2
  end
  dispenseReward = 0
end

% the transition trial - dont dispense
if (dispenseReward == 1 && iReward == nTransitionTrials) do
  iReward = iReward+1 % don't dispense - needs to now lick at the next one.
  disp('Animal receiving reward at')
  disp(lickWell)
  lastRewWell = lickWell
  dispenseReward = 0
end

% posttransition (lick needed) reward delivery
if (dispenseReward == 1 && iReward > nTransitionTrials) do
  iReward = iReward+1
  portout[lickWell] = 1  % Administer reward
  % output current program status:
  disp('Reward given at well ')
  disp(lickWell)
  disp(iReward)
  if (lickForWellEntryThisTrial < nLicksForWellEntry) do
    lickForWellEntryThisTrial = lickForWellEntryThisTrial+1
  end
  do in rewPumpOnTime % after the appropriate time
    portout[lickWell] = 0 % turn pump off
    lastRewWell = lickWell % where animal last got reward, not where dispensed - keeps the logic the same for fn 2
  end
  dispenseReward = 0
end
end;

%%%%%%%%%%%%%%%%%%%%
% output well interactions to screen
function 4
disp('Well #')
disp(lickWell)
disp('Left after:')
disp(lickCount)
disp(nonLickCount)
end;

%%%%%%%%%%%%%%%%%%%%
% User control of pumps - 50 is last fn
function 50
portout[pumpToTrig] = 1  % Trigger pump
do in rewPumpOnTime % after the appropriate time
    portout[pumpToTrig] = 0 % turn pump off
end
end;

%%%%%%%%%%%%%%%%%%%%%%%%%
%%   CALLBACK SECTION
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% Beambreak Up %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 1
callback portin[1] up
currWell = 1
oppWell = 2
% record time of beam break
beambreakStart = clock()

if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  lickCount = 0 % reset for this well
  nonLickCount = 0 % reset for this well
  lickWell = currWell
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 2
callback portin[2] up
currWell = 2
oppWell = 1
% record time of beam break
beambreakStart = clock()

if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  lickCount = 0 % reset for this well
  nonLickCount = 0 % reset for this well
  lickWell = currWell
end
end;

%%%%%%%%%%%%%% Beambreak Down %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 1
callback portin[1] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 2
callback portin[2] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(3) % dispense reward if necessary
end;

%% Trigger 50 for pump run. pumpToTrig is the var to choose which pump.
