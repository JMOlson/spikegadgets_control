%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%  Name:  Barrier Sounds - Just randomly move barriers up and down.
%%	Author: Jacob Olson
%%  Purpose: Habituate animals to barrier sounds. Run with animal in the room.
%%
%%  Reminder: All time values are in ms.
%%
%%  Notes:
%%    4 Barriers - Digital Outputs 17-20
%%
%%  Version: 1.2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIALIZE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ZERO OUT THE CLOCK! %%
clock(reset)

int nBars = 4;

int barrierPort1 = 17;
int barrierPort2 = 18;
int barrierPort3 = 19;
int barrierPort4 = 20;
int randMax = 120000;
int barMax = 4;

int nextBar = 1;
int nextBarTime = 60000;

int true = 1;

%%%%%%%%%%%%%%%%%%%%
function 1
while true == 1 do every nextBarTime
    if nextBar == 1 do
        portout[barrierPort1] = flip
    end
    if nextBar == 2 do
        portout[barrierPort2] = flip
    end
    if nextBar == 3 do
        portout[barrierPort3] = flip
    end
    if nextBar == 4 do
        portout[barrierPort4] = flip
    end
    nextBar = random(4)
    nextBarTime = random(5000)
end
end;

trigger(1);
