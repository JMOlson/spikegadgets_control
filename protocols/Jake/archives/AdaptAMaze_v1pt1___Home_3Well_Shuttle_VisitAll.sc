%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%  Name:  Home + 3 Well - Shuttle - Visit All - Lick Detect Well Entry
%%	Author: Jacob Olson
%%  Purpose: Home well + 3 Goal Wells. Alternate b/w Home and Goals
%%           Must visit all 3 goal wells before repeat rewards.
%%           Well visit determined by adequate number of licks.
%%           Used on Jake AdaptAMaze config.
%%
%%  Reminder: All time values are in ms.
%%
%%  Notes:
%%    Beam status is low when unoccupied, high when broken, and low again when the beam break ends.
%%    Pump #s and beambreak #s correspond to the same wells.
%%
%%  Version: 1.1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example Track and Well Layout (4 is home well)
%      2
%     / \
% 1	 /   \  3
%   Y  4  Y
%   |__|__|

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIALIZE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------
% Adjustable Parameters
% -------------------------
int nLicksForWellEntry = 2

% beambreak duration window that will register as a lick
int lickDurationMin = 25
int lickDurationMax = 150

% pump run time - my (JMO) Jadhav Lab pumps are set to dispense at a rate of 15 ml/mn = 0.25 ml/sec
int rewPumpOnTime = 300 % 0.075 ml

int homeWellID = 4

% -------------------------
% Parameters for triggering pumps on demand
% -------------------------
int pumpToTrig = 0

% -------------------------
% Behavior Trackers
% -------------------------
int well1RewReady = 1
int well2RewReady = 1
int well3RewReady = 1

int outsideRewReady = 1
int homeRewReady = 1
int currWellRewReady = 1 % used as a variable so all well callbacks are the same

int lastRewWell = 0
int lickWell = 0
int currWell = 0 % used as a variable so all well callbacks are the same

int iReward = 0
int dispenseReward = 0

int beambreakDuration = 0
int beambreakStart = 0
int beambreakEnd = 0

int lickCount = 0
int nonLickCount = 0

%% ZERO OUT THE CLOCK! %%
clock(reset)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HELPER FUNCTIONS (executed with ‘trigger(X)’, where X is the fn #)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% check if beambreak is a lick, add to lick or nonlick counts
function 1
beambreakDuration = beambreakEnd - beambreakStart
if (beambreakDuration > lickDurationMin && beambreakDuration < lickDurationMax) do
  % was a lick
  lickCount = lickCount+1
else do
  nonLickCount = nonLickCount+1
end
end;

%%%%%%%%%%%%%%%%%%%%
% determine if reward should be given for this event
function 2
if (lickCount == nLicksForWellEntry && currWellRewReady == 1) do
  dispenseReward = 1
  currWellRewReady = 0
else do
  dispenseReward = 0
end
end;

%%%%%%%%%%%%%%%%%%%%
% trigger reward if ready
function 3
if (dispenseReward == 1) do
  iReward = iReward+1
  portout[lickWell] = 1  % Administer reward
  % output current program status:
  disp(lickWell)
  disp(iReward)
  do in rewPumpOnTime % after the appropriate time
    portout[lickWell] = 0 % turn pump off
    lastRewWell = lickWell
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% output well interactions to screen, reset well variables
function 4
disp('Well #')
disp(lickWell)
disp('Left after:')
disp(lickCount)
disp(nonLickCount)

% reset things for this well
lickCount = 0
nonLickCount = 0
lickWell = currWell
disp(well1RewReady)
disp(well2RewReady)
disp(well3RewReady)
disp(outsideRewReady)
disp(homeRewReady)
end;

%%%%%%%%%%%%%%%%%%%%
% reset goal well states
function 5
if ((well1RewReady == 0) && (well2RewReady == 0) && (well3RewReady == 0))  do % all have been visited - reset.
  well1RewReady = 1
  well2RewReady = 1
  well3RewReady = 1
  disp('rewards reset')
end
end;

%%%%%%%%%%%%%%%%%%%%
% if well is officially visited, reset home/goal rew states
function 6
if (lickCount == nLicksForWellEntry) do
  if (lickWell == homeWellID) do
    outsideRewReady = 1
    homeRewReady = 0
  else do
    outsideRewReady = 0
    homeRewReady = 1
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% User control of pumps - 50 is last fn
function 50
portout[pumpToTrig] = 1  % Trigger pump
do in rewPumpOnTime % after the appropriate time
    portout[pumpToTrig] = 0 % turn pump off
end
end;

%%%%%%%%%%%%%%%%%%%%%%%%%
%%   CALLBACK SECTION
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% Beambreak Up %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 1
callback portin[1] up
currWell = 1
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry, reset well variables
  if ((well1RewReady == 1) && (outsideRewReady == 1)) do
  	currWellRewReady = 1
  else do
	currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 2
callback portin[2] up
currWell = 2
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry, reset well variables
  if ((well2RewReady == 1) && (outsideRewReady == 1)) do
  	currWellRewReady = 1
  else do
	currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 3
callback portin[3] up
currWell = 3
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  if ((well3RewReady == 1) && (outsideRewReady == 1)) do
  	currWellRewReady = 1
  else do
	currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 4  -- HOME
callback portin[4] up
currWell = 4
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  currWellRewReady = homeRewReady
  lickWell = currWell
end
end;

%%%%%%%%%%%%%% Beambreak Down %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 1
callback portin[1] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward == 1 do
  well1RewReady = 0
end
trigger(5) % reset goal wells if necessary
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 2
callback portin[2] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  well2RewReady = 0
end
trigger(5) % reset goal wells if necessary
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 3
callback portin[3] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  well3RewReady = 0
end
trigger(5) % reset goal wells if necessary
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 4 - HOME WELL
callback portin[4] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  homeRewReady = 0
end
trigger(3) % dispense reward if necessary
end;

%% Trigger 50 for pump run. pumpToTrig is the var to choose which pump.
