%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%  Name:  Home + 4 Well - Shuttle - Visit All - Lick Detect Well Entry - Barriers Set 13
%%	Author: Jacob Olson
%%  Purpose: Home well + 4 Goal Wells. Alternate b/w Home and Goals
%%           Must visit all 4 goal wells before repeat rewards.
%%           Well visit determined by adequate number of licks.
%%           Used on Jake AdaptAMaze config. After 5 visits to all wells, 2 barriers rise for the remainder of the session.
%%
%%          Barriers Used: 1 and 3.
%%
%%  Reminder: All time values are in ms.
%%
%%  Notes:
%%    Beam status is low when unoccupied, high when broken, and low again when the beam break ends.
%%    Pump #s and beambreak #s correspond to the same wells.
%%	V1.2 now uses smaller home rewards.
%%  V1.3 updates to pneumatic barriers on DIOs.
%%
%%   Version: 1.3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example Track and Well Layout (5 is home well)
% 1   / \   2
%   Y     Y
%   |  5  |
%  _|__|__|_
% |         |
% 3	        4


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIALIZE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------
% Barrier Locs
% -------------------------
int barrierConfig = 13

% -------------------------
% Adjustable Parameters
% -------------------------
int nLicksForWellEntry = 2

% beambreak duration window that will register as a lick
int lickDurationMin = 25
int lickDurationMax = 150

% pump run time - my (JMO) Jadhav Lab pumps are set to dispense at a rate of 15 ml/mn = 0.25 ml/sec
int rewPumpOnTime_Away = 300 % 0.075 ml
int rewPumpOnTime_Home = 250 % 0.0625 ml
int rewPumpOnTime = 0

int preBarrierLaps = 5

int homeWellID = 5

% -------------------------
% Parameters for triggering pumps on demand
% -------------------------
int pumpToTrig = 0

% -------------------------
% Hardware Mapping
% -------------------------
int barrierSignalAOut = 1
int barrierControlAOut = 2

int barrierPort1 = 17;
int barrierPort2 = 18;
int barrierPort3 = 19;
int barrierPort4 = 20;

% -------------------------
% Behavior Trackers
% -------------------------
int well1RewReady = 1
int well2RewReady = 1
int well3RewReady = 1
int well4RewReady = 1

int outsideRewReady = 1
int homeRewReady = 1
int currWellRewReady = 1 % used as a variable so all well callbacks are the same

int lastRewWell = 0
int lickWell = 0
int currWell = 0 % used as a variable so all well callbacks are the same

int iReward = 0
int dispenseReward = 0

int beambreakDuration = 0
int beambreakStart = 0
int beambreakEnd = 0

int lickCount = 0
int nonLickCount = 0

int lapsCompleted = 0
int barrierSetFlag = 0

%% ZERO OUT THE CLOCK! %%
clock(reset)

%% Ensure barriers start down
portout[17] = 0;
portout[18] = 0;
portout[19] = 0;
portout[20] = 0;
portout[21] = 0;
portout[22] = 0;
portout[23] = 0;
portout[24] = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HELPER FUNCTIONS (executed with ‘trigger(X)’, where X is the fn #)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% check if beambreak is a lick, add to lick or nonlick counts
function 1
beambreakDuration = beambreakEnd - beambreakStart
if (beambreakDuration > lickDurationMin && beambreakDuration < lickDurationMax) do
  % was a lick
  lickCount = lickCount+1
else do
  nonLickCount = nonLickCount+1
end
end;

%%%%%%%%%%%%%%%%%%%%
% determine if reward should be given for this event
function 2
if (lickCount == nLicksForWellEntry && currWellRewReady == 1) do
  dispenseReward = 1
  currWellRewReady = 0
else do
  dispenseReward = 0
end
end;

%%%%%%%%%%%%%%%%%%%%
% trigger reward if ready
function 3
if (dispenseReward == 1) do
  iReward = iReward+1
  portout[lickWell] = 1  % Administer reward
  % output current program status:
  disp(lickWell)
  disp(iReward)
  rewPumpOnTime = rewPumpOnTime_Away
  if (lickWell == homeWellID) do
	rewPumpOnTime = rewPumpOnTime_Home
  end
  do in rewPumpOnTime % after the appropriate time
    portout[lickWell] = 0 % turn pump off
    lastRewWell = lickWell
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% output well interactions to screen, reset well variables
function 4
disp('Well #')
disp(lickWell)
disp('Left after:')
disp(lickCount)
disp(nonLickCount)

% reset things for this well
lickCount = 0
nonLickCount = 0
lickWell = currWell
disp(well1RewReady)
disp(well2RewReady)
disp(well3RewReady)
disp(well4RewReady)
disp(outsideRewReady)
disp(homeRewReady)
end;

%%%%%%%%%%%%%%%%%%%%
% reset goal well states
function 5
if ((well1RewReady == 0) && (well2RewReady == 0) && (well3RewReady == 0) && (well4RewReady == 0))  do % all have been visited - reset.
  well1RewReady = 1
  well2RewReady = 1
  well3RewReady = 1
  well4RewReady = 1
  lapsCompleted = lapsCompleted+1
  if (lapsCompleted == preBarrierLaps) do
    barrierSetFlag = 1
  end
  disp('rewards reset')
end
end;

%%%%%%%%%%%%%%%%%%%%
% if well is officially visited, reset home/goal rew states
function 6
if (lickCount == nLicksForWellEntry) do
  if (currWell == homeWellID) do
    outsideRewReady = 1
    homeRewReady = 0
  else do
    outsideRewReady = 0
    homeRewReady = 1
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% User control of pumps - 50 is last fn
function 50
portout[pumpToTrig] = 1  % Trigger pump
rewPumpOnTime = rewPumpOnTime_Away
if (pumpToTrig == homeWellID) do
	rewPumpOnTime = rewPumpOnTime_Home
end
do in rewPumpOnTime % after the appropriate time
    portout[pumpToTrig] = 0 % turn pump off
end
end;

%%%%%%%%%%%%%%%%%%%%
% Move barriers to correct position.
function 49
if barrierSetFlag == 1 do
    barrierSetFlag = 0
if barrierConfig == 13 do
    portout[barrierPort1] = 1
    portout[barrierPort3] = 1
end
if barrierConfig == 14 do
    portout[barrierPort1] = 1
    portout[barrierPort4] = 1
end
if barrierConfig == 23 do
    portout[barrierPort2] = 1
    portout[barrierPort3] = 1
end
if barrierConfig == 24 do
    portout[barrierPort2] = 1
    portout[barrierPort4] = 1
end
end
end;

%%%%%%%%%%%%%%%%%%%%%%%%%
%%   CALLBACK SECTION
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% Beambreak Up %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 1
callback portin[1] up
currWell = 1
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry, reset well variables
  if ((well1RewReady == 1) && (outsideRewReady == 1)) do
  	currWellRewReady = 1
  else do
	currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 2
callback portin[2] up
currWell = 2
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry, reset well variables
  if ((well2RewReady == 1) && (outsideRewReady == 1)) do
  	currWellRewReady = 1
  else do
	currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 3
callback portin[3] up
currWell = 3
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  if ((well3RewReady == 1) && (outsideRewReady == 1)) do
  	currWellRewReady = 1
  else do
	currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 4
callback portin[4] up
currWell = 4
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  if ((well4RewReady == 1) && (outsideRewReady == 1)) do
  	currWellRewReady = 1
  else do
	currWellRewReady = 0
  end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 5  -- HOME
callback portin[5] up
currWell = 5
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
  % last port entry was elsewhere
  trigger(4) % record previous well entry
  currWellRewReady = homeRewReady
end
end;

%%%%%%%%%%%%%% Beambreak Down %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 1
callback portin[1] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward == 1 do
  well1RewReady = 0
end
trigger(5) % reset goal wells if necessary
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 2
callback portin[2] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  well2RewReady = 0
end
trigger(5) % reset goal wells if necessary
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 3
callback portin[3] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  well3RewReady = 0
end
trigger(5) % reset goal wells if necessary
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 4 - HOME WELL
callback portin[4] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  well4RewReady = 0
end
trigger(5) % reset goal wells if necessary
trigger(3) % dispense reward if necessary
end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 5 - HOME WELL
callback portin[5] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(2) % determine if reward should be given
trigger(6) % set correct home/away reward state
if dispenseReward  == 1 do
  homeRewReady = 0
end
trigger(3) % dispense reward if necessary
trigger(49)
end;

%% Trigger 50 for pump run. pumpToTrig is the var to choose which pump.
