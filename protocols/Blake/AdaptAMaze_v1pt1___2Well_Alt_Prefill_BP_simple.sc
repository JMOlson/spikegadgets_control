%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%  Name:  2 Well Alternation - Lick Detect Well Entry
%%	Author: Jacob Olson
%%  Purpose: 2 wells, reward when visited in alternation. Well visit determined
%%           by an adequate number of licks. Opposite well fills when a wells
%%           is visited (so reward is waiting for animal).
%%           Ideal for linear track on AdaptaMaze.
%%
%%  Quirks: Licks needed start at a lower value and increase +1 each trial until normal setting is reached.
%%  Reminder: All time values are in ms.
%%
%%  Notes:
%%    Beam status is low when unoccupied, high when broken, and low again when the beam break ends.
%%    Pump #s and beambreak #s correspond to the same wells.
%%
%%  Version: 1.1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIALIZE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------
% Parameters for triggering pumps on demand
% -------------------------
int pumpToTrig = 0

% -------------------------
% Adjustable Parameters
% -------------------------
int nLicksForWellEntry = 1
int lickForWellEntryThisTrial = 1 % increase over first few trials to the number above

% beambreak duration window that will register as a lick
int lickDurationMin = 25
int lickDurationMax = 150

% pump run time - my (JMO) Jadhav Lab pumps are set to dispense at a rate of 15 ml/mn = 0.25 ml/sec
int rewPumpOnTime = 300 % 0.075 ml
int homePumpOnTime = 600

% -------------------------
% Behavior Trackers
% -------------------------

int oppWell = 0
int currWell = 0 % used as a variable so all well callbacks are the same
int lickWell = 0

int currTime = 0
int iReward = 0
int dispenseReward = 0

int beambreakDuration = 0
int beambreakStart = 0
int beambreakEnd = 0

int lickCount = 0
int nonLickCount = 0

%% ZERO OUT THE CLOCK! %%
clock(reset)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HELPER FUNCTIONS (executed with ‘trigger(X)’, where X is the fn #)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% check if beambreak is a lick, add to lick or nonlick count

function 49
disp(currWell)
disp(lickWell)
disp(oppWell)
disp(lastRewWell)
end;

%%%%%%%%%%%%%%%%%%%%
% User control of pumps - 50 is last fn
function 50
portout[pumpToTrig] = 1  % Trigger pump
do in rewPumpOnTime % after the appropriate time
    portout[pumpToTrig] = 0 % turn pump off
end
end;

%%%%%%%%%%%%%%%%%%%%%%%%%
%%   CALLBACK SECTION
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% Beambreak Up %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 1
callback portin[3] up
currWell = 3
oppWell = 9
% record time of beam break
beambreakStart = clock()

if (lickWell != currWell) do
  lickWell = currWell
	portout[oppWell] = 1  % Administer reward
	disp('Reward at:')
	disp(oppWell)
	iReward = iReward+1
	do in homePumpOnTime % after the appropriate time
      	portout[oppWell] = 0 % turn pump off
	end
end
end;

%%%%%%%%%%%%%%%%%%%%
% Beam broken well 2
callback portin[10] up
currWell = 9
oppWell = 3
% record time of beam break
beambreakStart = clock()

if (lickWell != currWell) do
  lickWell = currWell
	portout[oppWell] = 1  % Administer reward
	disp('Reward at:')
	disp(oppWell)
	iReward = iReward+1
	do in rewPumpOnTime % after the appropriate time
      	portout[oppWell] = 0 % turn pump off
	end
end
end;

%%%%%%%%%%%%%% Beambreak Down %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 1
callback portin[3] down

end;

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 2
callback portin[10] down

end;

%% Trigger 50 for pump run. pumpToTrig is the var to choose which pump.
