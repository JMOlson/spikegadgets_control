%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%   Name: Grid 3x3 with 1 home well errorToHome
%%   Based on: Grid 3x3 with 1 home well rewardBias pt2
%%   Author: Blake Porter, modified from Jacob Olson
%%   Purpose: Grid maze where a home well flavor cue indicates to the rat where on the grid is rewarded
%%                    Home well pre-fills with a flavored milk after some delay. Once rat has sampled home well, a well 
%%                     on the grid will be able to dispense a reward if the rat licks 3x
%%           Used on Jake AdaptAMaze config.
%%
%%   Reminder: All time values are in ms.
%%
%%  errorToHome: rats must return home after an error, cannot keep searching grid
%%
%%  Notes:
%%    Beam status is low when unoccupied, high when broken, and low again when the beam break ends.
%%    Pump #s and beambreak #s correspond to the same wells.
%%
%%  Version: 1.0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example Track and Well Layout (10+ is home well)
% 
%              "Top"
%         
%       1-- --- -2- --- -3       
%        |          |          |
%       4 - --- -5- --- -6
%        |          |          |
%        7- --- -9- --- -10      door
%        |
%     |11|  pumps: 10,11, 12
%                           
% Computer
%
% pump 10 flavor 1, pump 11 flavor 2 etc at home well (11)

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIALIZE VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------
% Adjustable Parameters
% -------------------------
int nLicksForWellEntry = 2

% beambreak duration window that will register as a lick
int lickDurationMin = 20
int lickDurationMax = 200

% pump run time - my (JMO) Jadhav Lab pumps are set to dispense at a rate of 15 ml/mn = 0.25 ml/sec
int rewPumpOnTime = 500 % 
int homePumpOnTime = 400 % 


% -------------------------
% Parameters for triggering pumps on demand
% -------------------------
int pumpToTrig = 0

% -------------------------
% Behavior Trackers
% -------------------------
int currWellRewReady = 1 % used as a variable so all well callbacks are the same

int returnHome = 1 % 0 can go home, 1 can still be out
int trialState = 0 % 0 state rat should return to home location, 1 state rat should explore the grid

int lastRewWell = 0
int lickWell = 0
int currWell = 10 % used as a variable so all well callbacks are the same
int validWell = 0
int errorState = 1 % only count errors before a reward and not after a reward
int errorTracker = 0 % only count a well visit as one error, not num error = num licks
int prevWell = 0

int iFlavor_1 = 0 % num trials of this flavor
int iFlavor_2 = 0
int iFlavorDiff = 0
int iFlavor_1_rew = 0
int iFlavor_2_rew = 0
int iFlavorDiff_rew = 0
int iFlavor_1_consec = 0 % num consecutive trials this flavor
int iFlavor_2_consec = 0

int iFlavor_1_errors = 0
int iFlavor_2_errors = 0

int iStreak = 0
int iStreak_max = 0
int prevTrial = 0 % 0 wrong 1 correct
int movingAvg5 = 0

int flavorLimit = 5 % no one flavor should get more than 5 trials ahead of others
int flavorRewLimit = 5 % if one flavor is getting so many more rewards than other, force other trial

int currFlavor = random(1)+1
int currHomeFlavorPort = currFlavor+9

int iReward_total = 0
int iReward_grid = 0
int iReward_home = 0

int dispenseReward = 0

int beambreakDuration = 0
int beambreakStart = 0
int beambreakEnd = 0

int lickCount = 0
int nonLickCount = 0

int firstTrial = 0

int iGrid_1 = 0
int iGrid_2 = 0
int iGrid_3 = 0
int iGrid_4 = 0
int iGrid_5 = 0
int iGrid_6 = 0
int iGrid_7 = 0
int iGrid_8 = 0
int iGrid_9 = 0

int iGridRew_1 = 0
int iGridRew_2 = 0
int iGridRew_3 = 0
int iGridRew_4 = 0
int iGridRew_5 = 0
int iGridRew_6 = 0
int iGridRew_7 = 0
int iGridRew_8 = 0
int iGridRew_9 = 0

%% ZERO OUT THE CLOCK! %%
clock(reset)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HELPER FUNCTIONS (executed with ?trigger(X)?, where X is the fn #)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% check if beambreak is a lick, add to lick or nonlick counts
function 1
beambreakDuration = beambreakEnd - beambreakStart
if (beambreakDuration > lickDurationMin && beambreakDuration < lickDurationMax) do
  % was a lick
  lickCount = lickCount+1
else do
  nonLickCount = nonLickCount+1
end
end;

%%%%%%%%%%%%%%%%%%%%
% determine if reward should be given for this event
function 2
if (lickCount >= nLicksForWellEntry) do
	returnHome = 1 % can now return home for a new trial if rat wants
end

if (lickCount >= nLicksForWellEntry && currWellRewReady == 1) do
	
	if (validWell == 1 && trialState == 1) do
		dispenseReward = 1
		currWellRewReady = 0
	end
	currWellRewReady = 0
else do
  dispenseReward = 0
end
end;

%%%%%%%%%%%%%%%%%%%%
% trigger reward if ready
function 3
if (dispenseReward == 1) do
	iReward_total = iReward_total+1
	iReward_grid = iReward_grid+1
	portout[lickWell] = 1  % Administer reward
	dispenseReward = 0
	trialState = 0
	returnHome = 1
	errorState = 0
	iStreak = iStreak +1 
	if iStreak > iStreak_max do
		iStreak_max = iStreak
	end
	
	if movingAvg5 < 5 do
		movingAvg5 = movingAvg5+1
	end

  	% output current program status:
	disp('Reward dispensed at:')
	disp(lickWell)
	disp(iReward_grid)
	if currFlavor == 1 do
		iFlavor_1_rew = iFlavor_1_rew +1
	else do
		iFlavor_2_rew = iFlavor_2_rew+1 
	end

	do in rewPumpOnTime % after the appropriate time
		portout[lickWell] = 0 % turn pump off
		lastRewWell = lickWell
  	end
end
end;

%%%%%%%%%%%%%%%%%%%%
% output well interactions to screen, reset well variables
function 4
disp('Was at Well #')
disp(lickWell)
disp('Left after:')
disp(lickCount)
disp(nonLickCount)

% reset things for this well
lickCount = 0
nonLickCount = 0
validWell = 0
errorState = 1
errorTracker = 1
prevWell = lickWell
lickWell = currWell
disp('Now at Well #')
disp(currWell)
end;

%%%%%%%%%%%%%%%%%%%%
% home well reward dispense
function 5
iReward_total = iReward_total+1
iReward_home = iReward_home+1
trialState = 1 % Grid is now "active" and rat can get reward on it
returnHome = 0
currHomeFlavorPort = currFlavor+9
portout[currHomeFlavorPort] = 1  % Administer reward
lastRewWell = 10
errorState = 1

% output current program status:
lickCount = 0
nonLickCount = 0

disp(currFlavor)
disp(iReward_home)
disp(currHomeFlavorPort)
do in homePumpOnTime % after the appropriate time
	portout[currHomeFlavorPort] = 0 % turn pump off
end
end;

%%%%%%%%%%%%%%%%%%%%
%	Is this a rewarded well and are we on the rewarded trial type?
function 6
	validWell = 0
	if (currWell == 4 && currFlavor == 1) do
		validWell = 1
	else if (currWell == 8 && currFlavor == 2) do
		validWell = 1
	end

end;

% keep track of error visits per reward
function 7
if (lickCount >= nLicksForWellEntry && errorState == 1 && errorTracker == 1 && validWell == 0) do
	trialState = 0
	errorTracker = 0

		if (currWell == 4 || currWell == 8) do
			trialState = 1
		end
	
	if currWellRewReady == 0 do
		iStreak = 0
		if movingAvg5 > 0 do
			movingAvg5 = movingAvg5 -1
		end
	end
		
	if (currWell != 1 && currFlavor == 1) do
		iFlavor_1_errors = iFlavor_1_errors+1
		iStreak = 0
		if movingAvg5 > 0 do
			movingAvg5 = movingAvg5 -1
		end
	else if (currWell != 9 && currFlavor == 2) do
		iFlavor_2_errors = iFlavor_2_errors+1
		iStreak = 0
		if movingAvg5 > 0 do
			movingAvg5 = movingAvg5 -1
		end
	end
end
end;

function 46
disp(iFlavor_1)
disp(iFlavor_2)
disp(iFlavorDiff)
disp(iFlavor_1_rew)
disp(iFlavor_2_rew)
disp(iFlavorDiff_rew)
disp(iFlavor_1_consec)
disp(iFlavor_2_consec)
disp(iFlavor_1_errors)
disp(iFlavor_2_errors)
end;

%function 47
%disp(currWellRewReady)
%disp(trialState)
%disp(lickWell)
%disp(currWell)
%disp(validWell)
%disp(currFlavor)
%disp(dispenseReward)
%disp(lickCount)
%disp(nonLickCount)
%end;

function 48
disp(iGrid_1)
disp(iGrid_2)
disp(iGrid_3)
disp(iGrid_4)
disp(iGrid_5)
disp(iGrid_6)
disp(iGrid_7)
disp(iGrid_8)
disp(iGrid_9)
end;

function 49
disp(iGridRew_1)
disp(iGridRew_2)
disp(iGridRew_3)
disp(iGridRew_4)
disp(iGridRew_5)
disp(iGridRew_6)
disp(iGridRew_7)
disp(iGridRew_8)
disp(iGridRew_9)
end;


%%%%%%%%%%%%%%%%%%%%
% User control of pumps - 50 is last fn
function 50
portout[pumpToTrig] = 1  % Trigger pump
do in rewPumpOnTime % after the appropriate time
    portout[pumpToTrig] = 0 % turn pump off
end
end;

%%%%%%%%%%%%%%%%%%%%%%%%%
%%   CALLBACK SECTION
%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% Beambreak Up %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%
%  		Beam broken in home box
%
%%%%%%%%%%%%%%%%%%%%
callback portin[11] up
currWell = 10
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
end
if returnHome == 1 do
	disp('*~*~*~*~*~* New Trial *~*~*~*~*~*')
	returnHome = 0
	iFlavorDiff = iFlavor_2 - iFlavor_1
	iFlavorDiff_rew = iFlavor_2_rew - iFlavor_1_rew
	if (iFlavorDiff_rew < -3) || (iFlavorDiff_rew > 4) do
		disp('reward mismatch, forcing flavor')
		if iFlavorDiff_rew < -3 do 	
			currFlavor = 2
			iFlavor_2 = iFlavor_2+1
			iFlavor_1_consec = 0
			iFlavor_2_consec = 1
		else do
			currFlavor = 1
			iFlavor_1 = iFlavor_1+1
			iFlavor_1_consec = 1
			iFlavor_2_consec = 0
		end % if flavor rew <

%	else if (iFlavorDiff < -4) || (iFlavorDiff > 4) do
%		disp('num flavor mismatch, forcing flavor')
%		if iFlavorDiff < -4 do 	
%			currFlavor = 2
%			iFlavor_2 = iFlavor_2+1
%			iFlavor_1_consec = 0
%			iFlavor_2_consec = 1
%		else do
%			currFlavor = 1
%			iFlavor_1 = iFlavor_1+1
%			iFlavor_1_consec = 1
%			iFlavor_2_consec = 0
%		end % if flavor <

	else if (iFlavor_1_consec > 2) || (iFlavor_2_consec > 2) do
		disp('consecutive detected')
		if iFlavor_1_consec > 2 do
			currFlavor = 2
			iFlavor_2 = iFlavor_2+1
			iFlavor_1_consec = 0
			iFlavor_2_consec = 1
		else do
			currFlavor = 1
			iFlavor_1 = iFlavor_1+1
			iFlavor_1_consec = 1
			iFlavor_2_consec = 0
		end 	
	else do
		disp('rand flavor')
		currFlavor = random(1)+1 % i dont want flavor 0 to be confusing, so flavors start at 1
		if currFlavor == 1 do
			iFlavor_1 = iFlavor_1+1
			iFlavor_1_consec = iFlavor_1_consec +1
			iFlavor_2_consec = 0
		else do
			iFlavor_2_consec = iFlavor_2_consec +1
			iFlavor_1_consec = 0		
			iFlavor_2 = iFlavor_2+1
		end
	end
	trigger(5)
end
end;

%%%%%%%%%%%%%%%%%%%%
%
%     Beams broken on the grid
%
%    You just need to set currWellRewReady = 1 if you want them able to be rewarded
%
%%%%%%%%%%%%%%%%%%%%
% Beam broken well 1 - no reward possible
callback portin[1] up
currWell = 1
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_1 = iGrid_1+1
end
currWellRewReady = 0 % reward well 1 is not a valid locaiton so this will never be 1 for well 1
end;

% Beam broken well 2 - no reward possible
callback portin[2] up
currWell = 2
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_2 = iGrid_2+1
end
currWellRewReady = 0 % reward well 2 is not a valid locaiton so this will never be 1 for well 2
end;
 
% Beam broken well 3 - no reward possible
callback portin[3] up
currWell = 3
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_3 = iGrid_3+1
end
currWellRewReady = 0 % reward well 3 is not a valid locaiton so this will never be 1 for well 3
end;

% Beam broken well 4 - no reward possible
callback portin[4] up
currWell = 4
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_4 = iGrid_4+1
end
currWellRewReady = 1 % reward well 4 is not a valid locaiton so this will never be 1 for well 4
end;

% Beam broken well 5 - no reward possible
callback portin[5] up
currWell = 5
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_5 = iGrid_5+1
end
currWellRewReady = 0 % reward well 5 is not a valid locaiton so this will never be 1 for well 5
end;

% Beam broken well 6 - no reward possible
callback portin[6] up
currWell = 6
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_6 = iGrid_6+1
end
currWellRewReady = 0 % reward well 6 is a valid locaiton for reward
end;

% Beam broken well 7 - no reward possible
callback portin[7] up
currWell = 7
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_7 = iGrid_7+1
end
currWellRewReady = 0 % reward well 7 is not a valid locaiton so this will never be 1 for well 7
end;

% Beam broken well 8 - no reward possible
callback portin[9] up
currWell = 8
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_8 = iGrid_8+1
end
currWellRewReady = 1 % reward well 8 is a valid locaiton for reward
end;

% Beam broken well 9 - no reward possible
callback portin[10] up
currWell = 9
% record time of beam break
beambreakStart = clock()
if (lickWell != currWell) do
	trigger(4) % record previous well entry, reset well variables
	iGrid_9 = iGrid_9+1
end
currWellRewReady = 0 % reward well 9 is not a valid locaiton so this will never be 1 for well 9
end;


%%%%%%%%%%%%%% Beambreak Down %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Beam break ended well 1
callback portin[1] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_1 = iGridRew_1+1
end
trigger(3) % dispense reward if necessary
end;

% Beam break ended well 2
callback portin[2] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_2 = iGridRew_2+1
end
trigger(3) % dispense reward if necessary
end;

% Beam break ended well 3
callback portin[3] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_3 = iGridRew_3+1
end
trigger(3) % dispense reward if necessary
end;

% Beam break ended well 4
callback portin[4] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_4 = iGridRew_4+1
end
trigger(3) % dispense reward if necessary
end;

% Beam break ended well 5
callback portin[5] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_5 = iGridRew_5+1
end
trigger(3) % dispense reward if necessary
end;

% Beam break ended well 6
callback portin[6] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_6 = iGridRew_6+1
end
trigger(3) % dispense reward if necessary
end;

% Beam break ended well 7
callback portin[7] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_7 = iGridRew_7+1
end
trigger(3) % dispense reward if necessary
end;

% Beam break ended well 8
callback portin[9] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_8 = iGridRew_8+1
end
trigger(3) % dispense reward if necessary
end;

% Beam break ended well 9
callback portin[10] down
beambreakEnd = clock()
trigger(1) % handle end of beam break
trigger(6)
trigger(2) % determine if reward should be given
trigger(7)
if (dispenseReward == 1) do
	iGridRew_9 = iGridRew_9+1
end
trigger(3) % dispense reward if necessary
end;

%% Trigger 50 for pump run. pumpToTrig is the var to choose which pump.

